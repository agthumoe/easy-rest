package com.achromex.easyrest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Aung Thu Moe
 */
public class Assert<T> {
    private final ResponseEntity<T> responseEntity;

    private Assert(ResponseEntity<T> responseEntity) {
        this.responseEntity = responseEntity;
    }

    public Assert<T> hasStatus(int httpStatus) {
        assertThat(this.responseEntity.getStatusCodeValue()).isEqualTo(httpStatus);
        return this;
    }

    public Assert<T> hasStatus(HttpStatus httpStatus) {
        assertThat(this.responseEntity.getStatusCode()).isEqualTo(httpStatus);
        return this;
    }

    public Assert<T> hasContentType(MediaType mediaType) {
        assertThat(this.responseEntity.getHeaders().getContentType()).isEqualTo(mediaType);
        return this;
    }

    public Assert<T> hasHeader(String headerName, String headerValue) {
        assertThat(this.responseEntity.getHeaders().getValuesAsList(headerName)).contains(headerValue);
        return this;
    }

    public Assert<T> hasHeader(String headerName) {
        assertThat(this.responseEntity.getHeaders()).containsKey(headerName);
        return this;
    }

    public Assert<T> isEqualTo(T expected) {
        assertThat(this.responseEntity.getBody()).isEqualToComparingFieldByField(expected);
        return this;
    }

    public Assert<T> isEqualToComparingOnlyGivenFields(T expected, String... properties) {
        assertThat(this.responseEntity.getBody()).isEqualToComparingOnlyGivenFields(expected, properties);
        return this;
    }

    public Assert<T> hasFieldOrPropertyWithValue(String name, Object value) {
        assertThat(this.responseEntity.getBody()).hasFieldOrPropertyWithValue(name, value);
        return this;
    }

    public Assert<T> hasFieldOrProperty(String fieldOrProperty) {
        assertThat(this.responseEntity.getBody()).hasFieldOrProperty(fieldOrProperty);
        return this;
    }

    /**
     * @deprecated
     */
    public static <T> Assert<T> assertResponseEntity(ResponseEntity<T> actualResponse) {
        return new Assert<>(actualResponse);
    }

    public static <T> AssertResponseEntity<T> assertEntity(ResponseEntity<T> response) {
        return new AssertResponseEntity<>(response);
    }

    public static <T> AssertResponseEntityArray<T> assertEntityArray(ResponseEntity<T[]> response) {
        return new AssertResponseEntityArray<>(response);
    }
}
