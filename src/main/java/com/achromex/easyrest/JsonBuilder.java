package com.achromex.easyrest;

import java.util.Date;

public final class JsonBuilder {
    private final StringBuilder builder = new StringBuilder();

    private JsonBuilder() {
        builder.append("{");
    }

    public String build() {
        final String json = this.builder.append("}").toString();
        System.out.println("JSON Object: " + json);
        return json;
    }

    public JsonBuilder add(String key, String value) {
        if (builder.length() > 1) {
            builder.append(",");
        }
        builder.append("\"").append(key).append("\":").append("\"").append(value).append("\"");
        return this;
    }

    public JsonBuilder add(String key, Long value) {
        if (builder.length() > 1) {
            builder.append(",");
        }
        builder.append("\"").append(key).append("\":").append(value);
        return this;
    }

    public JsonBuilder add(String key, Date date) {
        return add(key, date.getTime());
    }

    public JsonBuilder add(String key, boolean value) {
        if (builder.length() > 1) {
            builder.append(",");
        }
        builder.append("\"").append(key).append("\":").append(value);
        return this;
    }

    public static JsonBuilder create() {
        return new JsonBuilder();
    }
}
