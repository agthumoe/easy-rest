package com.achromex.easyrest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Aung Thu Moe
 */
public class EasyRest<T> {
    private final ResponseEntity<T> responseEntity;

    private EasyRest(ResponseEntity<T> responseEntity) {
        this.responseEntity = responseEntity;
    }

    public EasyRest<T> hasStatus(int httpStatus) {
        assertThat(this.responseEntity.getStatusCodeValue()).isEqualTo(httpStatus);
        return this;
    }

    public EasyRest<T> hasStatus(HttpStatus httpStatus) {
        assertThat(this.responseEntity.getStatusCode()).isEqualTo(httpStatus);
        return this;
    }

    public EasyRest<T> hasContentType(MediaType mediaType) {
        assertThat(this.responseEntity.getHeaders().getContentType()).isEqualTo(mediaType);
        return this;
    }

    public EasyRest<T> hasHeader(String headerName, String headerValue) {
        assertThat(this.responseEntity.getHeaders().getValuesAsList(headerName)).contains(headerValue);
        return this;
    }

    public EasyRest<T> hasHeader(String headerName) {
        assertThat(this.responseEntity.getHeaders()).containsKey(headerName);
        return this;
    }

    public EasyRest<T> isEqualTo(T expected) {
        assertThat(this.responseEntity.getBody()).isEqualToComparingFieldByField(expected);
        return this;
    }

    public EasyRest<T> isEqualToComparingOnlyGivenFields(T expected, String... properties) {
        assertThat(this.responseEntity.getBody()).isEqualToComparingOnlyGivenFields(expected, properties);
        return this;
    }

    public EasyRest<T> hasFieldOrPropertyWithValue (String name, Object value) {
        assertThat(this.responseEntity.getBody()).hasFieldOrPropertyWithValue(name, value);
        return this;
    }

    public EasyRest<T> hasFieldOrProperty (String fieldOrProperty) {
        assertThat(this.responseEntity.getBody()).hasFieldOrProperty(fieldOrProperty);
        return this;
    }

    public static <T> EasyRest<T> assertResponseEntity(ResponseEntity<T> actualResponse) {
        return new EasyRest<T>(actualResponse);
    }
}
