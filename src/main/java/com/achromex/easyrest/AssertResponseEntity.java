package com.achromex.easyrest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

public class AssertResponseEntity<T> {
    private final ResponseEntity<T> responseEntity;

    public AssertResponseEntity(ResponseEntity<T> responseEntity) {
        this.responseEntity = responseEntity;
    }

    public AssertResponseEntity<T> hasStatus(int httpStatus) {
        assertThat(this.responseEntity.getStatusCodeValue()).isEqualTo(httpStatus);
        return this;
    }

    public AssertResponseEntity<T> hasStatus(HttpStatus httpStatus) {
        assertThat(this.responseEntity.getStatusCode()).isEqualTo(httpStatus);
        return this;
    }

    public AssertResponseEntity<T> hasContentType(MediaType mediaType) {
        assertThat(this.responseEntity.getHeaders().getContentType()).isEqualTo(mediaType);
        return this;
    }

    public AssertResponseEntity<T> hasHeader(String headerName, String headerValue) {
        assertThat(this.responseEntity.getHeaders().getValuesAsList(headerName)).contains(headerValue);
        return this;
    }

    public AssertResponseEntity<T> hasHeader(String headerName) {
        assertThat(this.responseEntity.getHeaders()).containsKey(headerName);
        return this;
    }

    public AssertResponseEntity<T> isEqualTo(T expected) {
        assertThat(this.responseEntity.getBody()).isEqualToComparingFieldByField(expected);
        return this;
    }

    public AssertResponseEntity<T> isEqualToComparingOnlyGivenFields(T expected, String... properties) {
        assertThat(this.responseEntity.getBody()).isEqualToComparingOnlyGivenFields(expected, properties);
        return this;
    }

    public AssertResponseEntity<T> hasFieldOrPropertyWithValue(String name, Object value) {
        assertThat(this.responseEntity.getBody()).hasFieldOrPropertyWithValue(name, value);
        return this;
    }

    public AssertResponseEntity<T> hasFieldOrProperty(String fieldOrProperty) {
        assertThat(this.responseEntity.getBody()).hasFieldOrProperty(fieldOrProperty);
        return this;
    }
}
