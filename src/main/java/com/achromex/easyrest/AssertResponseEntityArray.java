package com.achromex.easyrest;

import org.assertj.core.api.AbstractListAssert;
import org.assertj.core.api.ObjectAssert;
import org.assertj.core.groups.Tuple;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AssertResponseEntityArray<T> {
    private final ResponseEntity<T[]> responseEntity;

    public AssertResponseEntityArray(ResponseEntity<T[]> responseEntity) {
        this.responseEntity = responseEntity;
    }

    public AssertResponseEntityArray<T> hasSize(int expectedSize) {
        assertThat(this.responseEntity.getBody()).hasSize(expectedSize);
        return this;
    }

    public AbstractListAssert<?, List<? extends Tuple>, Tuple, ObjectAssert<Tuple>> extracting(String... propertiesOrFields) {
        return assertThat(this.responseEntity.getBody()).extracting(propertiesOrFields);
    }
}
