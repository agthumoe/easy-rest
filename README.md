# How to use
* Add repository in the repositories section.
* Add dependency in dependencies section.
## Add the repository
```xml
<repositories>
    ...
    <repository>
            <id>easy-rest</id>
            <name>Easy Rest</name>
            <releases>
                    <enabled>true</enabled>
            </releases>
            <snapshots>
                    <enabled>false</enabled>
            </snapshots>
            <url>https://raw.github.com/agthumoe/easy-rest.git/releases</url>
    </repository>
    ...
</repositories>
```
## Add depencency
```xml
<depencencies>
    <dependency>
        <groupId>com.achromex</groupId>
        <artifactId>easy-rest</artifactId>
        <version>1.0-RELEASE</version>
    </dependency>
</depencencies>
```
